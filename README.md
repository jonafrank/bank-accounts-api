# Bank Accounts API

## Requirements
* Docker
* docker-compose

## Run

1. clone repository
2. `cd bank-accounts-api`
3. `docker-compose up`

## API Documentation

### Create Account

**Endpoint:** `POST http://localhost:8080/api/accounts`
**Request Body Format:** 
```javascript
    {
        "clientName": String,
        "founds": Number
    }
```
**Response Format:** 
*Status Code:* `201 CREATED`
```javascript
{
    "id": String,
    "clientName": String,
    "founds": Number
}
```

### Get An Account
**Endpoint:** `GET http://localhost:8080/api/accounts/:accountId`

**Response Format:**
*Status Code:* `200 SUCCESS`
```javascript
{
    "id": String,
    "clientName": String,
    "founds": Number,
    "createdAt": String,
    "updatedAt": String,
}
```

### Create a Transaction
**Endpoint** `POST http://localhost:8080/api/accounts/:accountId/transactions`

**Request Format** 
```javascript
{
    "type": "credit" || "debit",
    "amount": Number
}
```
**Response Format:**
*Status Code:* `201 CREATED`
```javascript
{
    "id": String,
    "type": "credit" || "debit",
    "account": String,
    "amount": Number,
    "previousFounds": Number,
    "newFounds": Number,
    "createdAt": String,
}
```
**Error Responses:**
*Status Code:* `403 FORBIDDEN`
```javascript
{
    "error": "Forbidden",
    "message": "Account ###### has not enough founds to complete the transaction"
}
```
*Status Code:* `404 NOT FOUND`
```javascript
{
    "error": "Not Found"
    "message": "Account ###### not found"
}
```
