import express from 'express';
import validate from 'express-validation';
import * as validationSchemas from '../config/validationSchema';
import * as accounts from '../controllers/accountsController';
import * as transactions from '../controllers/transactionsController';

const router = express.Router();

router.post('/accounts', validate(validationSchemas.createAccount), accounts.createAccount);
router.get('/accounts/:accountId', accounts.getAccount);
router.post('/accounts/:accountId/transactions', validate(validationSchemas.createTransaction), transactions.createTransaction);
router.get('/accounts/:accountId/transactions', transactions.getTransactions);
export default router;