import uuid from 'uuid';
import moment from 'moment';
import httpStatus from 'http-status';
import redis from '../services/redis';
import ItemNotFound from '../errors/ItemNotFoundError';

export const createAccount = async (req, res) => {
    try {
        const accountId = uuid.v4();
        const now = moment().format('YYYY-MM-DD HH:mm:ss');
        await redis.hmsetAsync([
            `account:${accountId}`,
            'id',
            accountId,
            'client',
            req.body.clientName,
            'founds',
            req.body.founds,
            'createdAt',
            now,
            'updatedAt', 
            now
        ]);
        return res.status(httpStatus.CREATED).json({
            id: accountId,
            client: req.body.clientName,
            founds: req.body.founds,
            createdAt: now,
            updatedAt: now
        });
    } catch (err) {
        console.error(err);
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            error: 'Internal Server Error',
            message: 'Something went wrong saving the account'
        });
    }
}

export const getAccount = async (req, res) => {
    try {
        const accountId = req.params.accountId;
        const account = await  redis.hgetallAsync(`account:${accountId}`);
        if (!account) {
            throw new ItemNotFound(accountId)
        }
        return res.json(account);
    } catch (err) {
        if (err instanceof ItemNotFound) {
            return res.status(httpStatus.NOT_FOUND).json({
                error: 'Not Found',
                messge: `Account #${accountId} not found.`
            })
        }
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            error: 'Internal Server Error',
            message: 'Something went wrong'
        });
    }
}