import moment from 'moment';
import uuid from 'uuid';
import httpStatus from 'http-status';
import redis from '../services/redis';
import NotEnoughFounds from '../errors/NotEnoughFoundsErrors';
import ItemNotFound from '../errors/ItemNotFoundError';

export const createTransaction = async (req, res) => {
    try {
        const accountId = req.params.accountId;
        const amount = req.body.amount;
        const type = req.body.type;
        const account = await redis.hgetallAsync(`account:${accountId}`);
        if (!account) {
            throw new ItemNotFound(accountId);
        }
        const prevFounds = Number(account.founds);
        const now = moment().format('YYYY-MM-DD HH:mm:ss');
        let newFounds;
        switch (type) {
            case 'debit':
                if (prevFounds < amount) {
                    throw new NotEnoughFounds(accountId);
                }
                newFounds = prevFounds - amount;
                break;
            case 'credit':
                newFounds = prevFounds + amount;
                break;
            default:
                // For param validation it never should enter here
                throw new Error('Invalid Transaction type');
                
        }
        await redis.hmset(`account:${accountId}`, 'founds', newFounds, 'updatedAt', now);
        const transaction = {
            id: uuid.v4(),
            type,
            account: accountId,
            amount,
            previousFounds: prevFounds,
            newFounds,
            createdAt: now,
        }
        await redis.sadd(`transactions:${accountId}`, JSON.stringify(transaction));
        return res.status(httpStatus.CREATED).json(transaction);
    } catch (err) {
        console.error(err);
        if (err instanceof NotEnoughFounds) {
            return res.status(httpStatus.FORBIDDEN).json({
                error: 'Forbidden',
                message: `Account #${err.accountId} has not enough founds to complete the transaction`
            });
        }
        if (err instanceof ItemNotFound) {
            return res.status(httpStatus.NOT_FOUND).json({
                error: 'Not Found',
                messge: `Account #${accountId} not found.`
            })
        }
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            error: 'Internal Server Error',
            message: 'Something went wrong creating the transaction'
        });
    }t
}

export const getTransactions = async (req, res) => {
    try {
        const accountId = req.params.accountId; 
        const account = await redis.hgetallAsync(`account:${accountId}`);
        if (!account) {
            throw new ItemNotFound(id);
        }
        const rawTransactions = await redis.smembersAsync(`transactions:${accountId}`);
        if (!rawTransactions instanceof Array) {
            return res.json([]);
        }
        const transactions = rawTransactions.map(val => JSON.parse(val));
        return res.json(transactions);
    } catch (err) {
        if (err instanceof ItemNotFound) {
            return res.status(httpStatus.NOT_FOUND).json({
                error: 'Not Found',
                messge: `Account #${accountId} not found.`
            })
        }
        console.error(err);
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            error: 'Internal Server Error',
            message: 'Something went wrong'
        });
    }
}
