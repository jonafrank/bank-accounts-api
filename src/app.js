import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import config from './config';
import routes from './routes';

const app = express();

app.use(cors());
app.use(bodyParser.json());

app.use('/api/', routes);

app.listen(config.app.port, () => {
    console.info(`Bank Accounts API running on port ${config.app.port}`);
    console.info('------------------------------------------------------------');
});