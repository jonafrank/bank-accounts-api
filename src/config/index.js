const config = {
    app: {
        port: process.env.PORT || 8080
    },
    redis: {
        url: process.env.REDIS_URL
    }
}

export default config;