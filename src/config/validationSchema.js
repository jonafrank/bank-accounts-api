import Joi from 'joi';

export const createAccount  = {
    body: {
        clientName: Joi.string().required(),
        founds: Joi.number().required(),
    }
}

export const createTransaction = {
    body: {
        type: Joi.string().allow('credit', 'debit').required(),
        amount: Joi.number().required()
    }
}