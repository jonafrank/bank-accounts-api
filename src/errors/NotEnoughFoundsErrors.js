class NotEnoughFoundsError extends Error {
    constructor(accountId, ...args) {
        super(...args);
        this.accountId = accountId
    }
}

export default NotEnoughFoundsError;
