class ItemNotFoundError extends Error {
    constructor(id, ...args) {
        super(args);
        this.id = id;
    }
}

export default ItemNotFoundError;
