#!/bin/bash
if [[ $NODE_ENV = "development" ]]; then
    npm config set -g production false
fi

if [ ! -d "node_modules" ]; then
    npm install
fi
npm start
